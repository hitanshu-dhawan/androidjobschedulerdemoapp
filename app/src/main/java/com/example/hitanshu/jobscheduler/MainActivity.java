package com.example.hitanshu.jobscheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    private static final int HR24 = 24 * 60 * 60 * 1000;

    private ImageView backgroundImageView;
    private TextView quoteTextView;
    private TextView authorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        backgroundImageView = findViewById(R.id.background_image_view);
        quoteTextView = findViewById(R.id.quote_text_view);
        authorTextView = findViewById(R.id.author_text_view);

        initJobScheduler();

        loadData();
    }

    private void initJobScheduler() {
        JobInfo.Builder jobInfoBuilder = new JobInfo.Builder(0, new ComponentName(MainActivity.this, MyJobService.class));
//        jobInfoBuilder
//                .setPeriodic(HR24)
//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
//                .setRequiresDeviceIdle(true);
        jobInfoBuilder
                .setPeriodic(HR24 / 8)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(jobInfoBuilder.build());
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        quoteTextView.setText(sharedPreferences.getString("quote", "Quote"));
        authorTextView.setText("- " + sharedPreferences.getString("author", "Author"));
        if (sharedPreferences.contains("backgroundImageLink")) {
            Glide.with(MainActivity.this)
                    .load(sharedPreferences.getString("backgroundImageLink", ""))
                    .centerCrop()
                    .into(backgroundImageView);
        }
    }
}
