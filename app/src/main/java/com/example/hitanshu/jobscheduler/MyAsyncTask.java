package com.example.hitanshu.jobscheduler;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by hitanshu on 11/3/18.
 */

public class MyAsyncTask extends AsyncTask<String, Void, Quote> {

    private OnDownloadListner onDownloadListner;

    public MyAsyncTask(OnDownloadListner onDownloadListner) {
        this.onDownloadListner = onDownloadListner;
    }

    @Override
    protected Quote doInBackground(String... params) {
        if (isCancelled()) return null;
        try {
            URL url = new URL(params[0]);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() != 200) return null;

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream);
            String jsonString = "";
            while (scanner.hasNext()) {
                jsonString += scanner.nextLine();
            }

            // Parse JSON
            JSONObject jsonObject = new JSONObject(jsonString);
            String quote = jsonObject.getJSONObject("contents").getJSONArray("quotes").getJSONObject(0).getString("quote");
            String author = jsonObject.getJSONObject("contents").getJSONArray("quotes").getJSONObject(0).getString("author");
            String backgroundImageLink = jsonObject.getJSONObject("contents").getJSONArray("quotes").getJSONObject(0).getString("background");
            return new Quote(quote, author, backgroundImageLink);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Quote quote) {
        onDownloadListner.onDownloadComplete(quote);
    }

    public interface OnDownloadListner {
        void onDownloadComplete(Quote quote);
    }
}
