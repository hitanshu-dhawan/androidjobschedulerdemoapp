package com.example.hitanshu.jobscheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.SharedPreferences;

/**
 * Created by hitanshu on 11/3/18.
 */

public class MyJobService extends JobService {

    private MyAsyncTask myAsyncTask;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {

        myAsyncTask = new MyAsyncTask(new MyAsyncTask.OnDownloadListner() {
            @Override
            public void onDownloadComplete(Quote quote) {
                if (quote == null) {
                    jobFinished(jobParameters, true);
                    return;
                }

                SharedPreferences sharedPreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("quote", quote.getQuote());
                editor.putString("author", quote.getAuthor());
                editor.putString("backgroundImageLink", quote.getBackgroundImageLink());
                editor.apply();

                jobFinished(jobParameters, false);
            }
        });
        myAsyncTask.execute("https://quotes.rest/qod");

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        if (myAsyncTask != null) {
            myAsyncTask.cancel(true);
        }
        return true;
    }
}
