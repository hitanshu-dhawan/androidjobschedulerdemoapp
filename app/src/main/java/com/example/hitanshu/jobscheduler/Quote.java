package com.example.hitanshu.jobscheduler;

/**
 * Created by hitanshu on 11/3/18.
 */

public class Quote {

    private String quote;
    private String author;
    private String backgroundImageLink;

    public Quote(String quote, String author, String backgroundImageLink) {
        this.quote = quote;
        this.author = author;
        this.backgroundImageLink = backgroundImageLink;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBackgroundImageLink() {
        return backgroundImageLink;
    }

    public void setBackgroundImageLink(String backgroundImageLink) {
        this.backgroundImageLink = backgroundImageLink;
    }
}
